module PyPlotUtils

using PyCall
using PyPlot

export simpleaxis, plotblack

function simpleaxis(ax::PyObject=gca())
    ax[:spines]["top"][:set_visible](false)
    ax[:spines]["right"][:set_visible](false)
    ax[:get_xaxis]()[:tick_bottom]()
    ax[:get_yaxis]()[:tick_left]()
    ax[:minorticks_on]()
    return ax
end

# function plotblack(x, y; marker="", markevery="", clip_on=false, kwargs...)
function plotblack(x, y; label="", kwargs...)
    setting = Dict(kwargs)
    plot(x, y;
        color = "0",
        markerfacecolor = "white",
        zorder = 2,
        label = label,
        kwargs...,
    )
    plot(x, y;
        color = "0",
        markerfacecolor = "white",
        zorder = 3,
        kwargs...,
        linestyle = "",
    )
end

end # module
